const chromium = require('chrome-aws-lambda');
const puppeteer = require('puppeteer-core');
const locateChrome = require('locate-chrome');
 




 



exports.handler = async function(event,context){

    
const myexecutablePath = await new Promise(resolve => locateChrome(arg => resolve(arg)));
//const browser = await puppeteer.launch({ executablePath });
    const browser = await puppeteer.launch({
        args: chromium.args,
        //executablePath: process.env.CHROME_EXECUTABLE_PATH || await chromium.executablePath,
        executablePath: myexecutablePath || await chromium.executablePath,
        headless: true
    })

    const page = await browser.newPage();

    
    const keyword=event.multiValueQueryStringParameters.keyword;
     
    await page.goto('https://www.amazon.es/s?k='+keyword);
    

    const title = await page.title();
    await page.waitForSelector('div[class="s-main-slot s-result-list s-search-results sg-row"]');
    //.s-result-item
    //data-component-type="s-search-results"
    //data-component-type="s-search-result"
    const enlaces = await page.evaluate(() => {

         const productos = document.querySelectorAll('[data-component-type="s-search-result"] h2 a ');
         const imagenes = document.querySelector("#search > div.s-desktop-width-max.s-desktop-content.s-opposite-dir.sg-row > div.s-matching-dir.sg-col-16-of-20.sg-col.sg-col-8-of-12.sg-col-12-of-16 > div > span:nth-child(4) > div.s-main-slot.s-result-list.s-search-results.sg-row > div:nth-child(31) > div > div > div > div > div.s-product-image-container.aok-relative.s-image-overlay-grey.s-text-center.s-padding-left-small.s-padding-right-small.s-spacing-small.s-height-equalized > span > a > div > img")

         const precios = document.querySelectorAll(".a-price > span");
         const links = [];
         let i=0;
        for (let producto of productos) {
            i++;
             links.push(imagenes.src);
             links.push(producto.href);
             links.push(producto.text);
             links.push(precios[i].innerText);
            //  var el = document.querySelectorAll(".a-price > span");
            //  for(i=0; i<=el.length; i++){console.log(el[i].innerText)}
            
         }
        return links
    });


    await browser.close();

    return {
        statusCode: 200,
        body: JSON.stringify({
            status: 'Ok',
            arg: event.multiValueQueryStringParameters.keyword,
            title,
            links : enlaces
        })
    };
}

